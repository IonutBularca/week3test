#pragma once
#include "Card.h"
#include <vector>
#include "Deck.h"
class GameBoard
{
private:
	std::vector<Card> m_board;
	Deck* m_deck;
	bool validateSet(const Card& first, const Card& second, const Card& third);
public:
	GameBoard(Deck& deck);//initializare cu 12 carti
	GameBoard();
	~GameBoard();
	void init(Deck& deck);
	void printBoard() const;
	void add();
	bool set();
};

