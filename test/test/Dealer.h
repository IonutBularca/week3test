#pragma once
#include "GameBoard.h"
#include "Deck.h"
class Dealer
{
private:
	Deck m_deck;
	GameBoard m_gameBoard;

public:
	Dealer();
	~Dealer();
	void play();
};

