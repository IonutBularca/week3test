 #pragma once
#include <iostream>
class Card
{
public:
	//--------------
	enum class color
	{
		Red,
		Green,
		Blue
	};
	enum class symbol {
		Diamond,
		Squiggle,
		Oval
	};
	enum class shading {
		Solid,
		Stripped,
		Open
	};
	enum class number {
		One = 1,
		Two,
		Three
	};
	//--------------
	Card();
	Card(const number& number, const color& color, const shading& shading, const symbol& sym);

	friend std::ostream& operator << (std::ostream& os, const Card& card);
	~Card();
	int getNumber() const;
	int getColor() const;
	int getShading() const;
	int getSymbol() const ;
private:

	number m_number;
	color m_color;
	shading m_shading;
	symbol m_symbol;
};

