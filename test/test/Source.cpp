#include "Card.h"
#include "Deck.h"
#include "GameBoard.h"
int main() {
	Deck deckTest;
	deckTest.shuffle();
	GameBoard boardTest(deckTest);
	boardTest.printBoard();
	boardTest.add();
	std::cout << deckTest.cards_num() << std::endl;
}