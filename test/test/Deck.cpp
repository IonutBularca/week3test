#include "Deck.h"
#include <random>
#include <algorithm>

void Deck::shuffle()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, 80);
	for (size_t swapIndex = 0; swapIndex < m_deck.size(); ++swapIndex)
	{
		size_t swapPosition = dis(gen);
		Card aux;
		aux = m_deck[swapIndex];
		m_deck[swapIndex] = m_deck[swapPosition];
		m_deck[swapPosition] = aux;
	}

}

Deck::Deck():m_cardIndex(0)
{
	m_deck.resize(81);
	int index = 0;
	for (int i = static_cast<int>(Card::number::One); i <= static_cast<int>(Card::number::Three); i++) {
		for (int j = static_cast<int>(Card::color::Red); j <= static_cast<int>(Card::color::Blue); j++) {
			for (int k = static_cast<int>(Card::shading::Solid); k <= static_cast<int>(Card::shading::Open); k++) {
				for (int t = static_cast<int>(Card::symbol::Diamond); t <= static_cast<int>(Card::symbol::Oval); t++) {
					m_deck[index] = { static_cast<Card::number>(i), static_cast<Card::color>(j),static_cast<Card::shading>(k),static_cast<Card::symbol>(t) };
					index++;
				}
			}
		}
	}
}


Deck::~Deck()
{
}

void Deck::test_print()
{
	for (int i = 0; i < 81; i++) {
		std::cout << m_deck[i] << std::endl;
	}
}

Card Deck::extractCard()
{
	if (m_cardIndex < m_deckSize - 1)
	{
		++m_cardIndex;
	}
	return m_deck[m_cardIndex];
}

bool Deck::empty()
{
	return (m_cardIndex == m_deckSize - 1);
}

size_t Deck::cards_num()
{
	return (m_deckSize-m_cardIndex);
}
