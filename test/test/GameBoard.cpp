#include "GameBoard.h"



bool validateNumber(const Card& card1, const Card& card2, const Card& card3) {
	if (card1.getNumber() != card2.getNumber() && card1.getNumber() != card3.getNumber() && card2.getNumber() != card3.getNumber())
		return true;
	return false;
}
bool validateColors(const Card& card1, const Card& card2, const Card& card3) {
	if (card1.getColor() != card2.getColor() && card1.getColor() != card3.getColor() && card2.getColor != card3.getColor())
		return true;
	return false;
}
bool validateSymbols(const Card& card1, const Card& card2, const Card& card3) {
	if (card1.getSymbol() != card2.getSymbol() && card1.getSymbol() != card3.getSymbol() && card2.getSymbol != card3.getSymbol())
		return true;
	return false;
}
bool validateShades(const Card& card1, const Card& card2, const Card& card3) {
	if (card1.getShading() != card2.getShading() && card1.getShading() != card3.getShading() && card2.getShading() != card3.getShading())
		return true;
	return false;
}


bool GameBoard::validateSet(const Card & first, const Card & second, const Card & third)
{
	if (validateColors(first, second, third)) {
		if (validateSymbols(first, second, third)) {
			if (validateNumber(first, second, third)) {
				if (validateShades(first, second, third)) {
					//----
					return true;
				}
			}
		}
	}

	return false;
}

GameBoard::GameBoard(Deck & deck)
{
	while (m_board.size() < 12) {
		m_board.push_back(deck.extractCard());
	}
	//nu am facut nicio verificare pentru ca la inceputul jocului vom avea un pachet plin, deci sigur nu se va goli 
	//prin extragerea a 12 carti
	m_deck = &deck;
}

GameBoard::GameBoard()
{
}


GameBoard::~GameBoard()
{
}

void GameBoard::init(Deck& deck)
{
	while (m_board.size() < 12) {
		m_board.push_back(deck.extractCard());
	}
	m_deck = &deck;
}

void GameBoard::printBoard() const
{
	for (auto v : m_board) {
		std::cout << v << std::endl;
	}
}

void GameBoard::add()
{
	if (!m_deck->empty()) {
		m_board.push_back(m_deck->extractCard());
		m_board.push_back(m_deck->extractCard());
		m_board.push_back(m_deck->extractCard());
	}
}

bool GameBoard::set()
{
	for (int i = 0; i < m_board.size()-2; i++) {
		for (int j = i + 1; j < m_board.size()-1; j++) {
			for (int k = j + 1; k < m_board.size(); k++) {
				if (validateSet(m_board[i], m_board[j], m_board[k])) {
					//do something

					m_board[i] = m_deck->extractCard();
					m_board[j] = m_deck->extractCard();
					m_board[k] = m_deck->extractCard();
					return true;
				}
			}
		}
	}
	//-------------------------
	add();
	return false;
}
