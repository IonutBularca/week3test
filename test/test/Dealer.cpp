#include "Dealer.h"






Dealer::Dealer()
{
	m_deck.shuffle();
	m_gameBoard.init(m_deck);
}

Dealer::~Dealer()
{
}

void Dealer::play()
{
	int playersNumber;
	int currentPlayer = 0;
	std::vector<int> scores;
	for (int i = 0; i < playersNumber; i++) {
		scores.push_back(0);
	}
	std::cout << "Number of players: ";
	std::cin >> playersNumber;
	while (!m_deck.empty()) {
		system("cls");
		char choice;
		m_gameBoard.printBoard();
		std::cout << "Set: yes(y)/no(n)";
		std::cin >> choice;
		if (choice == 'y') {
			if (m_gameBoard.set()) {
				scores[currentPlayer]++;
			}
			else {
				scores[currentPlayer]--;
				currentPlayer = (currentPlayer + 1) % playersNumber;
			}
		}
		system("pause");
	}
	system("cls");
	for (auto v : scores) {
		std::cout << v << " ";
	}
}
