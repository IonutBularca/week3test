#pragma once
#include "Card.h"
#include <vector>
#include <iostream>
class Deck
{
private:
	std::vector<Card> m_deck;
	size_t m_cardIndex;//this will help us to get a card from the deck
	static const int m_deckSize = 81;
public:
	void shuffle();
	Deck();
	~Deck();
	void test_print();
	Card extractCard();
	bool empty();
	size_t cards_num();
};

