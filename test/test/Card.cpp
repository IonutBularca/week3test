#include "Card.h"



Card::Card()
{
}

Card::Card(const number & number, const color & color, const shading & shading, const symbol & sym):
	m_number(number), m_color(color), m_shading(shading), m_symbol(sym)
{

}


Card::~Card()
{
}

int Card::getNumber() const
{
	return  static_cast<int>(m_number);
}

int Card::getColor() const
{
	return static_cast<int>(m_color);
}

int Card::getShading() const
{
	return static_cast<int>(m_shading);
}

int Card::getSymbol() const
{
	return static_cast<int>(m_symbol);
}

std::ostream& operator<<(std::ostream & os, const Card & card)
{
	os << static_cast<int>(card.m_number);
	os << " ";
	switch (card.m_color) {
		case Card::color::Red :
		os << "Red "; break;
		case Card::color::Blue:
		os << "Blue "; break;
		case Card::color::Green:
		os << "Green "; break;
	}
	switch (card.m_symbol) {
	case Card::symbol::Diamond:
		os << "Diamond "; break;
	case Card::symbol::Oval:
		os << "Oval "; break;
	case Card::symbol::Squiggle:
		os << "Squiggle "; break;
	}
	switch (card.m_shading) {
	case Card::shading::Solid:
		os << "Solid "; break;
	case Card::shading::Stripped:
		os << "Stripped "; break;
	case Card::shading::Open:
		os << "Open "; break;
	}
	return os;
}
